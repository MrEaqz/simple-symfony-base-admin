<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController extends AbstractController
{
    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;

    public function __construct(CsrfTokenManagerInterface $csrfTokenManager, UserPasswordHasherInterface $encoder, ValidatorInterface $validator)
    {
        $this->csrfTokenManager = $csrfTokenManager;
        $this->encoder = $encoder;
        $this->validator = $validator;
    }

    /**
     * @Route("/login", name="auth/login")
     */
    public function login(Request $request): Response
    {
        if ($request->isMethod('post')) {
            $credentials = [
                'email' => $request->request->get('email'),
                'password' => $request->request->get('password'),
                'csrf_token' => $request->request->get('csrf_token'),
            ];

            $token = new CsrfToken('authenticate', $credentials['csrf_token']);
            if ($this->csrfTokenManager->isTokenValid($token)) {
                throw new InvalidCsrfTokenException();
            }

            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $credentials['email']]);

            if ($this->encoder->isPasswordValid($user, $credentials['password'])) {
                // Auth Success
            } else {
                // Auth Failed
            }
        }

        
        return $this->render('auth/login.html.twig', [
            'controller_name' => 'AuthController',
        ]);
    }

    /**
     * @Route("/register", name="auth/register")
     */
    public function register(Request $request): Response
    {
        if ($request->isMethod('post')) {

            $entityManager = $this->getDoctrine()->getManager();

            $credentials = [
                'username' => $request->request->get('username'),
                'email' => $request->request->get('email'),
                'password' => $request->request->get('password'),
                'csrf_token' => $request->request->get('csrf_token'),
                'roles' => ['USER']
            ];

            $token = new CsrfToken('authenticate', $credentials['csrf_token']);
            if ($this->csrfTokenManager->isTokenValid($token)) {
                throw new InvalidCsrfTokenException();
            }

            $user = new User();
            $user->setUsername($credentials['username']);
            $user->setEmail($credentials['email']);
            $user->setPassword($this->encoder->hashPassword($user, $credentials['password']));
            $user->setRoles($credentials['roles']);
            
            $entityManager->persist($user);
            $entityManager->flush();

            if ($user->getId()) {
                // Registration complete with successful.
                $this->addFlash('success', 'Account created with id n°'. $user->getId() .' !');
                return $this->redirect("/login");
            } else {
                $this->addFlash('error', 'An error occurred during the registration process. Please, try again.');
            }
        }
        return $this->render('auth/register.html.twig', [
            'controller_name' => 'AuthController',
        ]);
    }

    /**
     * @Route("/logout", name="auth/logout")
     */
    public function logout(): Response
    {
        return $this->redirect("/login");
    }

}
